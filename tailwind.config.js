module.exports = {
    content: ["./src/**/*.{html,js,tsx,jsx}"],
    theme: {
        extend: {
            colors: {
                mainBlue: "#1F74C2",
                orangeGraph: "#F29F23",
                lightGray: "#5B5B5B",
                success: "#258E00",
                fail: "#CB0000",
                input: "#F2F5FF",
                bgGray: "#EBEBEB",
            },
            boxShadow: {
                inputLine: "0px 4px 4px rgba(0, 0, 0, 0.25);",
            },
        },
    },
    plugins: [],
};
