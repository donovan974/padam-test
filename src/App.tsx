import Busy from "./components/Busy";
import BookingContextProvider from "./context/BookingContext";

function App() {
    return (
        <div>
            <BookingContextProvider>
                <Busy />
            </BookingContextProvider>
        </div>
    );
}

export default App;
