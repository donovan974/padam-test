import { useState } from "react";

export type RequestStatus = "loading" | "success" | "error";

/**
 * @brief Hook to handle requests with a status.
 * @param request - The function to call to make the request
 * @returns a finction to execute, a status and the request data
 */
const useRequest = <P, D>(request: (param: P) => Promise<D | undefined>) => {
    const [status, setStatus] = useState<RequestStatus>("loading");
    const [data, setData] = useState<D | undefined>(undefined);

    const emptyData = async () => setData(undefined);

    const execute = async (param: P) => {
        emptyData();
        setStatus("loading");
        request(param)
            .then((res) => {
                setStatus("success");
                setData(res);
            })
            .catch((err) => {
                setStatus("error");
                setData(undefined);
            });
    };

    return [execute, status, data] as [
        typeof execute,
        RequestStatus,
        D | undefined
    ];
};

export default useRequest;
