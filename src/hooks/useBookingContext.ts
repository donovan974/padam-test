import { useContext } from "react";
import { BookingContext } from "../context/BookingContext";

/**
 * @returns the context value of bookingContext
 */
const useBookingContext = () => {
    return useContext(BookingContext);
};

export default useBookingContext;
