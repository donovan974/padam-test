import { useEffect, useState } from "react";
import { getAllTrips } from "../requests/trip";
import { GetTripsParams, ResultSelectorMode, TripItem } from "../types/trips";
import useRequest from "./useRequest";

/**
 * @brief Get every trips in the api or filter with params.
 * @param departure - The departure stop selected
 * @param arrival - The arrival stop selected
 * @returns fetched query result
 */
const useTripSelector = (departure: string, arrival: string) => {
    const [execute, status, data] = useRequest<GetTripsParams, TripItem[]>(
        getAllTrips
    );
    const [mode, setMode] = useState<ResultSelectorMode>("any");

    const exec = async () => {
        if (departure === "ANY" && arrival === "ANY") {
            await execute({ key: undefined, value: undefined });
            setMode("any");
        } else if (departure !== "ANY" && arrival === "ANY") {
            await execute({ key: "departureStop", value: departure });
            setMode("onlyDeparture");
        } else if (departure === "ANY" && arrival !== "ANY") {
            await execute({ key: "arrivalStop", value: arrival });
            setMode("onlyArrival");
        } else {
            await execute({ key: "arrivalStop", value: arrival });
            setMode("both");
        }
    };

    useEffect(() => {
        exec();
    }, [departure, arrival]);

    return [status, data, mode] as [typeof status, typeof data, typeof mode];
};

export default useTripSelector;
