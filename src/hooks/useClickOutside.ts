import { useRef, useEffect } from "react";

/**
 * @brief  Hook to detect click outside of a component and trigger a callback
 * @param callback - function to call when clicked outside of the element
 * @returns a reference to the element
 */
const useClickOutside = <T extends HTMLElement = HTMLDivElement>(
    callback: (event?: MouseEvent) => void
) => {
    const elementRef = useRef<T>(null);

    useEffect(() => {
        const handler = (event: MouseEvent) =>
            elementRef.current &&
            !elementRef.current.contains(event.target as Node) &&
            callback(event);

        document.addEventListener("mousedown", handler);
        return () => document.removeEventListener("mousedown", handler);
    }, [callback]);

    return elementRef;
};

export default useClickOutside;
