import { useEffect } from "react";
import { getAllStops } from "../requests/trip";
import useRequest from "./useRequest";

const useStops = () => {
    const [execute, status, data] = useRequest<void, string[]>(getAllStops);

    useEffect(() => {
        execute();
    }, []);

    return [status, data] as [typeof status, typeof data];
};

export default useStops;
