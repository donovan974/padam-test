import { useEffect, useState } from "react";

/**
 * @brief Hooks to get a value after a delay.
 * @param value - The value to debounce
 * @param delay - The delay in milliseconds
 * @returns a value of type T after a delay
 */
const useDebounceValue = <T>(value: T, delay: number) => {
    const [debouncedValue, setDebouncedValue] = useState<T>(value);

    useEffect(() => {
        const timeoutId = setTimeout(() => setDebouncedValue(value), delay);
        return () => clearTimeout(timeoutId);
    }, [value, delay]);

    return debouncedValue;
};

export default useDebounceValue;
