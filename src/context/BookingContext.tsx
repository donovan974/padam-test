import React, { createContext, useState } from "react";
import BookingWrapper from "../components/BookingWrapper";
import { TripItem } from "../types/trips";

type BookingContextValues = {
    bookThis: (id: TripItem) => void;
};

const emptyTrip = {
    id: "65",
    departureStop: "Paris",
    departureTime: "22H45",
    arrivalStop: "Orléan",
    arrivalTime: "7H50",
};

export const BookingContext = createContext<BookingContextValues>(null!);

type BookingContextProviderProps = { children?: React.ReactNode };

const BookingContextProvider = ({ children }: BookingContextProviderProps) => {
    const [isOpen, setIsOpen] = useState(false);
    const [trip, setTrip] = useState<TripItem>(emptyTrip);

    const bookThis = (trip: TripItem) => {
        setIsOpen(true);
        setTrip(trip);
    };

    const closeBooking = () => {
        setIsOpen(false);
        setTrip(emptyTrip);
    };

    return (
        <BookingContext.Provider value={{ bookThis }}>
            <BookingWrapper
                onCancelBook={closeBooking}
                isOpen={isOpen}
                trip={trip}
            >
                {children}
            </BookingWrapper>
        </BookingContext.Provider>
    );
};

export default BookingContextProvider;
