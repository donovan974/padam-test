import { GetTripsParams } from "../types/trips";

const BASE_ENDPOINT = process.env.REACT_APP_API_BASE_ENDPOINT;

/**
 * @brief Get every trips in the api or filter with params.
 * @param key - The key to search for
 * @param value - The value of the key
 * @returns the list of trips
 */
const getAllTrips = async ({
    key = undefined,
    value = undefined,
}: GetTripsParams) => {
    const response = await fetch(
        `${BASE_ENDPOINT}/trips${!key && !value ? "" : `?${key}=${value}`}`
    );
    return response.json();
};

/**
 * @brief get every stops in the API to search trips
 * @returns the list of stops
 */
const getAllStops = async () => {
    const response = await fetch(`${BASE_ENDPOINT}/stops`, { method: "GET" });
    return response.json();
};

/**
 * @brief book a trip using it's id
 * @param tripId - The id of the trip to book
 * @returns success or not
 */
const book = async (tripId: string) => {
    const response = await fetch(`${BASE_ENDPOINT}/book/${tripId}`, {
        method: "PUT",
    });
    return response.json();
};

export { getAllTrips, getAllStops, book };
