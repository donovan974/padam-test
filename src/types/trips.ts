export type TripItem = {
    id: string;
    departureStop: string;
    departureTime: string;
    arrivalStop: string;
    arrivalTime: string;
};

export type Stops = String[];

export type TripsSection = {
    name: string;
    trips: TripItem[];
};

export type GetTripsParams = {
    key: string | undefined;
    value: string | undefined;
};

export type ResultSelectorMode =
    | "onlyDeparture"
    | "onlyArrival"
    | "both"
    | "any";
