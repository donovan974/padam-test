import { useState } from "react";
import "./CheckBox.css";

type Props = {
    onStateChange?: (state: boolean) => void;
    defaultState?: boolean;
};

const CheckBox = ({ onStateChange, defaultState = false }: Props) => {
    const [state, setState] = useState(defaultState);

    const handleClick = () => {
        setState(!state);
        onStateChange && onStateChange(!state);
    };

    return (
        <button
            onClick={handleClick}
            className={`checkBox ${state ? "checkedBox" : "unCheckedBox"}`}
        />
    );
};

export default CheckBox;
