import React, { useEffect, useState } from "react";
import { GrClose } from "react-icons/gr";
import useRequest from "../../hooks/useRequest";
import { book } from "../../requests/trip";
import { TripItem } from "../../types/trips";
import BookingForm from "../BookingForm";
import BookingResult from "../BookingResult";
import Loader from "../Loader";

type Props = {
    children: React.ReactNode;
    isOpen?: boolean;
    onCancelBook: () => void;
    trip: TripItem;
};

type bookingWrapperMode = "booking" | "loading" | "fail" | "success";

const BookingWrapper = ({
    children,
    isOpen = false,
    onCancelBook,
    trip,
}: Props) => {
    const [bookingMode, setBookingMode] =
        useState<bookingWrapperMode>("booking");
    const [email, setEmail] = useState("");
    const [exec, status, data] = useRequest<string, { success: boolean }>(book);

    useEffect(() => {
        setEmail("");
        setBookingMode("booking");
    }, [isOpen]);

    useEffect(() => {
        if (bookingMode === "booking") return;
        if (status === "success" && data?.success === true)
            setBookingMode("success");
        if (status === "success" && data?.success === false)
            setBookingMode("fail");
        if (status === "error") setBookingMode("fail");
    }, [status, bookingMode, data]);

    const comfirmBooking = async (
        email: string,
        _fname: string,
        _lname: string
    ) => {
        setBookingMode("loading");
        setEmail(email);
        exec(trip.id);
    };

    return (
        <div className="relative">
            {children}
            {isOpen && (
                <div className="absolute bg-opacity-30 top-0 left-0 h-screen w-screen bg-black">
                    {bookingMode === "booking" && (
                        <BookingForm
                            onCancel={onCancelBook}
                            onConfirm={comfirmBooking}
                        />
                    )}
                    {bookingMode === "success" && (
                        <BookingResult
                            styles={"success"}
                            payload={{ email: email, trip }}
                        />
                    )}
                    {bookingMode === "fail" && (
                        <BookingResult
                            styles={"fail"}
                            payload={{ email: email, trip }}
                        />
                    )}
                    {bookingMode === "loading" && (
                        <div className="w-full h-full grid place-content-center">
                            <Loader />
                        </div>
                    )}
                    <GrClose className="absolute top-0 right-0 mr-5 mt-3 text-xl cursor-pointer " />
                </div>
            )}
        </div>
    );
};

export default BookingWrapper;
