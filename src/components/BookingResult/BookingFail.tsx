import { IoIosCheckmarkCircleOutline } from "react-icons/io";
import Button from "../Button";

type Props = {};

const BookingFail = (props: Props) => {
    return (
        <div className="formsBase">
            <div className="flex items-center text-fail">
                <IoIosCheckmarkCircleOutline className="text-5xl" />
                <h1 className="text-2xl">Oups.. Something went wrong !</h1>
            </div>
            <p className="italic">
                An error occurred while your travel tried to be booked. Try to
                reload the page or contact an administrator if the problem
                persists.
            </p>
            <Button
                text="RELOAD PAGE"
                onClick={() => window.location.reload()}
            />
        </div>
    );
};

export default BookingFail;
