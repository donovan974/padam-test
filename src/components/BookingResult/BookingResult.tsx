import { TripItem } from "../../types/trips";
import BookingFail from "./BookingFail";
import BookingSuccess from "./BookingSuccess";

type Props = {
    styles: "success" | "fail";
    payload: { trip: TripItem; email: string };
};

const BookingResult = ({ styles, payload }: Props) => {
    switch (styles) {
        case "fail":
            return <BookingFail />;
        case "success":
            return <BookingSuccess {...payload} />;
        default:
            return <BookingFail />;
    }
};

export default BookingResult;
