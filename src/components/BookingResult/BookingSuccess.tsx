import { TripItem } from "../../types/trips";
import { IoIosCheckmarkCircleOutline } from "react-icons/io";
import LabelWrapper from "../TripSection/LabelWrapper";
import Button from "../Button";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";

type Props = { trip: TripItem; email: string };

function BookingSuccess({ trip, email }: Props) {
    const departure = moment(trip.departureTime).format("dddd d MMMM HH:mm");
    const dtime = moment(trip.departureTime).format("HH:mm");
    const atime = moment(trip.arrivalTime).format("HH:mm");

    return (
        <div className="formsBase">
            <div className="flex items-center text-success">
                <IoIosCheckmarkCircleOutline className="text-5xl" />
                <h1 className="text-2xl">Your travel has been booked !</h1>
            </div>
            <h2 className="text-xl">TRAVEL RECAP</h2>
            <div>
                <LabelWrapper inline label="date:">
                    <p>{departure}</p>
                </LabelWrapper>
                <LabelWrapper inline label="Departure at:">
                    <p>
                        {trip.departureStop} - {dtime}
                    </p>
                </LabelWrapper>
                <LabelWrapper inline label="Arrival at:">
                    {trip.arrivalStop} - {atime}
                </LabelWrapper>
                <LabelWrapper inline label="Booking reference:">
                    {uuidv4().split("-")[0]}
                </LabelWrapper>
            </div>
            <p className="text-center">
                An email within all details has been sent <br /> at{" "}
                {email === "" ? "fake_email@gmail.com" : email}.
            </p>
            <Button text="OK THANKS" onClick={() => window.location.reload()} />
        </div>
    );
}

export default BookingSuccess;
