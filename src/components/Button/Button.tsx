import { BsChevronRight } from "react-icons/bs";
import "./Button.css";

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
    text: string;
};

const Button = ({ text, ...other }: Props) => {
    return (
        <button {...other} className="mainButtonStyle">
            <p className="font-bold mr-5">{text}</p>
            <BsChevronRight className="stroke-[1px] text-xl" />
        </button>
    );
};

export default Button;
