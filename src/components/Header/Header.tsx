import "./header.css";

const Header = () => {
    return (
        <div className="headerBackground shadow-lg ">
            <img
                src="./assets/LogoBusy.png"
                className="sm:w-auto w-52 h-auto self-center ml-10 pt-5"
                alt="logo"
            />
        </div>
    );
};

export default Header;
