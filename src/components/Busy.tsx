import { useMemo, useState } from "react";
import useStops from "../hooks/useStops";
import useTripSelector from "../hooks/useTripSelector";
import { filteredByMode } from "../utils/selectorFilter";
import Header from "./Header";
import Loader from "./Loader";
import Selector from "./Selector";
import TripSection from "./TripSection";
import { FiWifiOff } from "react-icons/fi";

const Busy = () => {
    const [departureStop, setDepartureStop] = useState<"ANY" | string>("ANY");
    const [arrivalStop, setArrivalStop] = useState<"ANY" | string>("ANY");
    const [status, data, mode] = useTripSelector(departureStop, arrivalStop);
    const [, stopData] = useStops();

    const splittedSection = useMemo(
        () => filteredByMode(data, mode, departureStop),
        [data, mode, departureStop]
    );

    return (
        <div className="h-screen w-screen overflow-auto bg-bgGray">
            <div className="relative h-fit">
                <Header />
                <div className="absolute bottom-0 left-1/2 -translate-x-1/2 translate-y-[30%]">
                    <Selector
                        stops={!stopData ? [] : stopData}
                        onDepartureSelect={setDepartureStop}
                        onArrivalSelect={setArrivalStop}
                    />
                </div>
            </div>
            <section className="w-full mt-20 flex flex-col items-center justify-center space-y-10 pb-20">
                {splittedSection.map((section) => (
                    <TripSection key={section.name} section={section} />
                ))}
                {splittedSection.length === 0 && status === "success" && (
                    <p className="text-xl italic">No results for this query.</p>
                )}
                {status === "error" && (
                    <div className="grid place-items-center space-y-5">
                        <p className="text-xl italic">
                            Error fail to tech data from server. Try later
                        </p>
                        <FiWifiOff className="text-5xl" />
                    </div>
                )}
                {status === "loading" && <Loader />}
            </section>
        </div>
    );
};

export default Busy;
