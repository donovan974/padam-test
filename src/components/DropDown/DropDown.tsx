import { useEffect, useState } from "react";
import { BsChevronDown } from "react-icons/bs";
import DropDownItem from "./DropDownItem";
import useClickOutside from "../../hooks/useClickOutside";
import "./DropDown.css";

type Props = {
    values: string[];
    onSelect: (value: string) => void;
};

const DropDown = ({ values, onSelect }: Props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [currentSelected, setCurrentSelected] = useState("ANY");
    const [items, setItems] = useState(values);

    useEffect(() => {
        const valueWithNone = ["ANY", ...values];
        setItems(valueWithNone);
    }, [values]);

    const DDItemsRef = useClickOutside(() => {
        setIsOpen(false);
    });

    const handleClickDD = () => {
        setIsOpen(!isOpen);
    };

    const handleClickDDItem = (value: string) => {
        onSelect(value);
        setCurrentSelected(value);
        setIsOpen(false);
    };

    return (
        <div
            ref={DDItemsRef}
            onClick={handleClickDD}
            className="dropDownContainer px-2 relative"
        >
            <p className="truncate">{currentSelected}</p>
            <BsChevronDown />
            <div className="dropDrownItemContainer">
                {isOpen &&
                    items.map((value, i) => (
                        <DropDownItem
                            key={i}
                            onClick={handleClickDDItem}
                            value={value}
                        />
                    ))}
            </div>
        </div>
    );
};

export default DropDown;
