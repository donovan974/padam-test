import React from "react";

type Props = { value: string; onClick: (value: string) => void };

const DropDownItem = ({ value, onClick }: Props) => {
    const handleClick = (
        e: React.MouseEvent<HTMLParagraphElement, MouseEvent>
    ) => {
        e.stopPropagation();
        onClick(value);
    };

    return (
        <p
            onClick={handleClick}
            className="hover:underline cursor-pointer px-2"
        >
            {value}
        </p>
    );
};

export default DropDownItem;
