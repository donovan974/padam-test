import DropDown from "../DropDown";
import "./Selector.css";

type Props = {
    stops?: string[];
    onDepartureSelect: (str: string) => void;
    onArrivalSelect: (str: string) => void;
    onChange?: () => void;
};

const Selector = ({
    stops = [],
    onDepartureSelect,
    onArrivalSelect,
    onChange,
}: Props) => {
    const handleDepartureSelection = (str: string) => {
        onDepartureSelect(str);
        onChange && onChange();
    };

    const handleArrivalSelection = (str: string) => {
        onArrivalSelect(str);
        onChange && onChange();
    };

    return (
        <div className="w-fit px-5">
            <div className="flex justify-around">
                <p className="headerSelector">DEPARTURE</p>
                <p className="headerSelector">ARRIVAL</p>
            </div>
            <div className="bg-white flex rounded-full px-10 py-3 shadow-lg">
                <DropDown values={stops} onSelect={handleDepartureSelection} />
                <div className="flex flex-col items-center mx-5">
                    <p>AND</p>
                    <hr className="border-t w-full h-[1px] border-black" />
                    <p>OR</p>
                </div>
                <DropDown values={stops} onSelect={handleArrivalSelection} />
            </div>
        </div>
    );
};

export default Selector;
