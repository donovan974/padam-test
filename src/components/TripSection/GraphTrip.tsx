import React from "react";
import GraphDot from "./GraphDot";

type Props = {
    departureStop: string;
    departureTime: string;
    arrivalStop: string;
    arrivalTime: string;
};

const GraphTrip = ({
    departureStop,
    departureTime,
    arrivalStop,
    arrivalTime,
}: Props) => {
    return (
        <div className="text-lg">
            <div className="sm:-mb-2 mb-0">
                <GraphDot
                    alt="Departure date and time"
                    char="D"
                    name={departureStop}
                    hours={departureTime}
                />
            </div>
            <div className="hidden sm:block sm:h-[4rem]  w-2 bg-orangeGraph ml-[10px]" />
            <div className="sm:-mt-3 mt-1">
                <GraphDot
                    alt="Arrival date and time"
                    char="A"
                    name={arrivalStop}
                    hours={arrivalTime}
                />
            </div>
        </div>
    );
};

export default GraphTrip;
