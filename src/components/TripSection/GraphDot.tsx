type Props = {
    char: string;
    name: string;
    hours: string;
    alt?: string;
};

const GraphDot = ({ char, name, hours, alt = "" }: Props) => {
    return (
        <div className="flex items-center space-x-2 h-fit w-fit">
            <div title={alt} className="graphDotDot">
                {char}
            </div>
            <p className="font-bold ">
                {name}
                <span className="text-opacity-60 text-black italic ml-2">
                    at {hours}
                </span>
            </p>
        </div>
    );
};

export default GraphDot;
