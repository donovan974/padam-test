import moment from "moment";
import { MdOutlineTimer } from "react-icons/md";
import { getRandomInt } from "../../utils/random";
import { getDifferenceTime } from "../../utils/time";
import Button from "../Button";
import GraphTrip from "./GraphTrip";
import LabelWrapper from "./LabelWrapper";
import "./CardTrip.css";
import useBookingContext from "../../hooks/useBookingContext";

type Props = {
    departureStop: string;
    departureTime: string;
    arrivalStop: string;
    arrivalTime: string;
    id: string;
};

const CardTrip = ({
    departureStop,
    departureTime,
    arrivalStop,
    arrivalTime,
    id,
}: Props) => {
    const dateTimeDeparture = new Date(departureTime);
    const dateTimeArrival = new Date(arrivalTime);
    const diff = getDifferenceTime(dateTimeArrival, dateTimeDeparture);
    const departure = moment(departureTime).format("dddd d MMMM HH:mm");
    const dtime = moment(departureTime).format("HH:mm");
    const atime = moment(arrivalTime).format("HH:mm");
    const obj = useBookingContext();

    return (
        <div className="cardTripContainer p-4">
            <div>
                <GraphTrip
                    arrivalStop={arrivalStop}
                    arrivalTime={atime}
                    departureStop={departureStop}
                    departureTime={dtime}
                />
            </div>
            <div className="flex flex-col justify-between">
                <LabelWrapper label="travel date:">
                    <p>{departure}</p>
                </LabelWrapper>
                <LabelWrapper label="duration:">
                    <div className="flex items-center">
                        <MdOutlineTimer className="text-2xl mr-1" />
                        <p>
                            {diff.hour}H{diff.min}
                        </p>
                    </div>
                </LabelWrapper>
            </div>
            <div className="flex flex-col justify-between sm:items-end">
                <LabelWrapper label="remaining seats:" inline>
                    <p>{getRandomInt(150)}</p>
                </LabelWrapper>
                <div className="flex justify-center mt-2 sm:block sm:m-0">
                    <Button
                        onClick={() =>
                            obj.bookThis({
                                arrivalStop,
                                arrivalTime,
                                departureStop,
                                departureTime,
                                id,
                            })
                        }
                        text="BOOK IT NOW"
                    />
                </div>
            </div>
        </div>
    );
};

export default CardTrip;
