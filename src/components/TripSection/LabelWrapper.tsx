import React from "react";

type Props = {
    children: React.ReactNode;
    label?: string;
    inline?: boolean;
};

const LabelWrapper = ({ children, label = "", inline = false }: Props) => {
    return (
        <div
            className={`text-gray-700 italic text-lg ${
                inline ? "flex items-center space-x-1" : ""
            } `}
        >
            <h3 className="font-bold not-italic text-base uppercase text-black">
                {label}
            </h3>
            {children}
        </div>
    );
};

export default LabelWrapper;
