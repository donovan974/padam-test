import { TripsSection } from "../../types/trips";
import CardTrip from "./CardTrip";

type Props = {
    section: TripsSection;
};

const TripSection = ({ section }: Props) => {
    return (
        <div className="w-full flex justify-center">
            <div className="w-[90%] lg:w-[80%] max-w-[1200px]">
                <h2 className="text-3xl mb-2">{section.name}</h2>
                <div className="space-y-5">
                    {section.trips.map((trip) => (
                        <CardTrip key={trip.id} {...trip} />
                    ))}
                </div>
            </div>
        </div>
    );
};

export default TripSection;
