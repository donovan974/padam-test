import React from "react";

type Props = {
    label: string;
    onChange?: (value: string) => void;
};

const LabeledInput = ({ onChange, label }: Props) => {
    return (
        <div>
            <h2 className="text-xl uppercase">{label}</h2>
            <input
                className="border text-lg border-black shadow-inputLine h-10 w-full pl-2 bg-input"
                onChange={(e) => onChange && onChange(e.target.value)}
            />
        </div>
    );
};

export default LabeledInput;
