import React, { useState } from "react";
import Button from "../Button";
import CheckBox from "../CheckBox";
import LabeledInput from "./LabeledInput";
import { GrClose } from "react-icons/gr";

type Props = {
    onConfirm: (email: string, fname: string, lname: string) => void;
    onCancel: () => void;
};

const BookingForm = ({ onConfirm, onCancel }: Props) => {
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    return (
        <div className="formsBase">
            <div className="w-[60%] space-y-5">
                <LabeledInput label="first name *" onChange={setFirstName} />
                <LabeledInput label="last name *" onChange={setLastName} />
                <LabeledInput label="e-mail adress *" onChange={setEmail} />
            </div>
            <div className="flex space-x-3 w-[60%]">
                <CheckBox />
                <p>
                    On checking this box you will ensure that you read and
                    agreed to{" "}
                    <a
                        className="text-blue-600 hover:underline"
                        href="https://padam-mobility.com/mentions-legales/"
                    >
                        the general term of use.
                    </a>
                    *
                </p>
            </div>
            <p className="italic w-[60%]">* required field</p>
            <div className="flex justify-between w-[60%] items-end">
                <button
                    className="mb-3 text-lightGray cursor-pointer hover:underline"
                    onClick={onCancel}
                >
                    No Thanks
                </button>
                <Button
                    text="COMFIRM BOOK"
                    onClick={() => onConfirm(email, firstName, lastName)}
                />
            </div>
            <GrClose
                onClick={onCancel}
                className="absolute top-0 right-0 mr-5 mt-3 text-xl cursor-pointer "
            />
        </div>
    );
};

export default BookingForm;
