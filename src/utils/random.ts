export const getRandomInt = (maxValue: number) => {
    return Math.floor(Math.random() * maxValue);
};
