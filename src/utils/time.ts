export const getDifferenceTime = (t1: Date, t2: Date) => {
    var diff = Math.abs(t1.getTime() - t2.getTime());

    var ms = diff;
    var hour = Math.floor(ms / 1000 / 60 / 60);
    ms -= hour * 1000 * 60 * 60;
    var min = Math.floor(ms / 1000 / 60);
    ms -= min * 1000 * 60;
    var sec = Math.floor(ms / 1000);
    ms -= sec * 1000;

    return { hour, min, sec };
};
