import { ResultSelectorMode, TripItem, TripsSection } from "../types/trips";

export const filteredByMode = (
    trips: TripItem[] | undefined,
    mode: ResultSelectorMode,
    departure: string
) => {
    if (!trips) return [];
    switch (mode) {
        case "onlyDeparture":
            return getSectionByArrival(trips);
        case "onlyArrival":
            return getSectionByArrival(trips);
        case "both":
            return getSectionByArrival(
                trips.filter((trip) => trip.departureStop === departure)
            );
        case "any":
            return getSectionByArrival(trips);
    }
};

const getSectionByArrival = (trips: TripItem[]) => {
    const sections: TripsSection[] = [];
    trips.forEach((trip) => {
        const section = sections.find(
            (section) => section.name === trip.arrivalStop
        );
        if (!section) sections.push({ name: trip.arrivalStop, trips: [trip] });
        else section.trips.push(trip);
    });
    return sections;
};
