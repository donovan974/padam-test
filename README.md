<h1>A propos du projet</h1>

Ce projet a était réalisé avec les technologies suivantes:

-   React
-   Tailwind
-   uuid
-   moment
-   Figma

⏱️ Temps:

-   Figma => 4H
-   Setup projet => 35 minutes
-   Développement => 4H
-   Peaufinage => 2H

Pour voir le design rdv sur <a href="https://www.figma.com/file/NsTMgLaNi47HLUZflFDAZV/PADAM-Test-technique?node-id=0%3A1" > ce lien sur Figam</a>

<h1>Comment le lancer ?</h1>
Pour lancer le projet:

```console
$> npm i
$> npm run build:css
$> npm run start
```

<h1>Un aperçu du projet sans le lancer</h1>
Voici un lien vers <a href='https://www.youtube.com/watch?v=aSQfx6BhOiU&feature=youtu.be'>une vidéo youtube</a>.
